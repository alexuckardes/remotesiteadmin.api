﻿namespace RemoteSiteAdmin.API.Requests
{
    public class CreateRemoteSiteRequest
    {
        public string RemoteSiteName { get; set; }
        public string UserName {get;set;}
    }
}