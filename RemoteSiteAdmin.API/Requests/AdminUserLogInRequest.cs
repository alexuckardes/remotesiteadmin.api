﻿namespace RemoteSiteAdmin.API.Requests
{
    public class AdminUserLogInRequest
    {
        public string UserName { get; set; }
        public string PassWord { get; set; }
    }
}