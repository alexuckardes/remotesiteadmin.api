﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RemoteSiteAdmin.API.Responses
{
    public abstract class ResponseBase
    {
        public List<string> Errors { get; protected set; }

        private bool _hasErrorSet;
        public bool HasError
        {
            get
            {
                if (_hasErrorSet) return true;
                if (Errors.Any()) return true;
                return false;
            }
            set { _hasErrorSet = value; }
        }

        protected ResponseBase()
        {
            Errors = new List<string>();
        }
    }
}