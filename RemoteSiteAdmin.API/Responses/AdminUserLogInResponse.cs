﻿namespace RemoteSiteAdmin.API.Responses
{
    public class AdminUserLogInResponse
    {
        public bool IsSucceed { get; set; }
        public string Message { get; set; }
        public string Url { get; set; }
    }
}