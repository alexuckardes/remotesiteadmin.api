﻿namespace RemoteSiteAdmin.API.Responses
{
    public class CreateRemoteSiteResponse : ResponseBase
    {
        public string Messages { get; set; }
    }
}