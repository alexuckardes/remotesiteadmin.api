﻿using Autofac;
using Autofac.Builder;

namespace RemoteSiteAdmin.API
{
    public abstract class AbstractModuleConfig : Module
    {
        public bool AppHasPerRequestLifetimeSupport { get; private set; }

        protected AbstractModuleConfig(bool appHasPerRequestLifetimeSupport)
        {
            AppHasPerRequestLifetimeSupport = appHasPerRequestLifetimeSupport;
        }
    }

    public static class ModuleConfigExtensions
    {
        public static IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> InstancePerRequestOrLifetimeScope<TLimit, TActivatorData, TRegistrationStyle>(this IRegistrationBuilder<TLimit, TActivatorData, TRegistrationStyle> builder, bool appHasPerRequestLifetimeSupport)
        {
            return appHasPerRequestLifetimeSupport ? builder.InstancePerRequest() : builder.InstancePerLifetimeScope();
        }
    }
}