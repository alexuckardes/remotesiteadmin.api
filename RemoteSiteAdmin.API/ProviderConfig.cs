﻿using Autofac;
using RemoteSiteAdmin.API.Managers;
using RemoteSiteAdmin.API.Providers;

namespace RemoteSiteAdmin.API
{
    public class ProviderConfig : AbstractModuleConfig
    {
        public ProviderConfig(bool appHasPerRequestLifetimeSupport)
            : base(appHasPerRequestLifetimeSupport)
        {

        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<RemoteSiteManager>()
                .AsSelf()
                .As<IRemoteSiteManager>()
                .InstancePerRequestOrLifetimeScope(AppHasPerRequestLifetimeSupport);
            builder.RegisterType<RemoteSiteRepository>()
                .AsSelf()
                .As<IRemoteSiteRepository>()
                .InstancePerRequestOrLifetimeScope(AppHasPerRequestLifetimeSupport);
        }
    }
}