﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RemoteSiteAdmin.API.Utilities
{
    public static class Helper
    {
        public static char EnCodeForRemoteSite(char character)
        {
            if (character == 'A') return '2'; if (character == 'a') return 'm';
            if (character == 'B') return 'E'; if (character == 'b') return 't';
            if (character == 'C') return '7'; if (character == 'c') return 'p';
            if (character == 'D') return '8'; if (character == 'd') return 'a';
            if (character == 'E') return '4'; if (character == 'e') return 'r';
            if (character == 'F') return '3'; if (character == 'f') return 'k';
            if (character == 'G') return 'Z'; if (character == 'g') return 'z';
            if (character == '1') return 'D'; if (character == '2') return 'A';
            if (character == '3') return 'G'; if (character == '4') return 'B';
            if (character == '5') return 'U'; if (character == '6') return '5';
            if (character == '7') return 'J'; if (character == '8') return '9';
            if (character == '9') return 'V'; if (character == '0') return '6';
            if (character == 'H') return 'M'; if (character == 'h') return 'c';
            if (character == 'I') return 'L'; if (character == 'i') return 'o';
            if (character == 'J') return 'C'; if (character == 'j') return 'h';
            if (character == 'K') return 'S'; if (character == 'k') return 'u';
            if (character == 'L') return 'I'; if (character == 'l') return 'e';
            if (character == 'M') return 'F'; if (character == 'm') return 'w';
            if (character == 'N') return 'P'; if (character == 'n') return 'd';
            if (character == 'O') return 'H'; if (character == 'o') return 'n';
            if (character == 'P') return 'R'; if (character == 'p') return 'f';
            if (character == 'R') return 'N'; if (character == 'r') return 'v';
            if (character == 'S') return 'K'; if (character == 's') return 'i';
            if (character == 'T') return 'Y'; if (character == 't') return 'b';
            if (character == 'U') return '0'; if (character == 'u') return 's';
            if (character == 'V') return '1'; if (character == 'v') return 'q';
            if (character == 'Y') return 'T'; if (character == 'y') return 'j';
            if (character == 'Z') return 'O'; if (character == 'z') return 'l';
            if (character == 'W') return 'Q'; if (character == 'w') return 'y';
            if (character == 'Q') return 'W'; if (character == 'q') return 'g';
            if (character == '.') return '='; if (character == '@') return '{';
            if (character == '&') return '|'; if (character == '/') return ',';
            if (character == ':') return 'x'; if (character == '=') return ';';

            return character;
        }

        public static string EnCodeForRemoteSite(string word)
        {
            string result = "";

            for (int i = 0; i < word.Length; i++)
            {
                result += EnCodeForRemoteSite(word[i]);
            }

            return result;
        }

        public static char DeCodeForRemoteSite(char character)
        {
            if (character == '2') return 'A'; if (character == 'm') return 'a';
            if (character == 'E') return 'B'; if (character == 't') return 'b';
            if (character == '7') return 'C'; if (character == 'p') return 'c';
            if (character == '8') return 'D'; if (character == 'a') return 'd';
            if (character == '4') return 'E'; if (character == 'r') return 'e';
            if (character == '3') return 'F'; if (character == 'k') return 'f';
            if (character == 'Z') return 'G'; if (character == 'z') return 'g';
            if (character == 'D') return '1'; if (character == 'A') return '2';
            if (character == 'G') return '3'; if (character == 'B') return '4';
            if (character == 'U') return '5'; if (character == '5') return '6';
            if (character == 'J') return '7'; if (character == '9') return '8';
            if (character == 'V') return '9'; if (character == '6') return '0';
            if (character == 'M') return 'H'; if (character == 'c') return 'h';
            if (character == 'L') return 'I'; if (character == 'o') return 'i';
            if (character == 'C') return 'J'; if (character == 'h') return 'j';
            if (character == 'S') return 'K'; if (character == 'u') return 'k';
            if (character == 'I') return 'L'; if (character == 'e') return 'l';
            if (character == 'F') return 'M'; if (character == 'w') return 'm';
            if (character == 'P') return 'N'; if (character == 'd') return 'n';
            if (character == 'H') return 'O'; if (character == 'n') return 'o';
            if (character == 'R') return 'P'; if (character == 'f') return 'p';
            if (character == 'N') return 'R'; if (character == 'v') return 'r';
            if (character == 'K') return 'S'; if (character == 'i') return 's';
            if (character == 'Y') return 'T'; if (character == 'b') return 't';
            if (character == '0') return 'U'; if (character == 's') return 'u';
            if (character == '1') return 'V'; if (character == 'q') return 'v';
            if (character == 'T') return 'Y'; if (character == 'j') return 'y';
            if (character == 'O') return 'Z'; if (character == 'l') return 'z';
            if (character == 'Q') return 'W'; if (character == 'y') return 'w';
            if (character == 'W') return 'Q'; if (character == 'g') return 'q';
            if (character == '{') return '@'; if (character == '=') return '.';
            if (character == '|') return '&'; if (character == ',') return '/';
            if (character == 'x') return ':'; if (character == ';') return '=';

            return character;
        }

        public static string DeCodeForRemoteSite(string word)
        {
            string result = "";

            for (int i = 0; i < word.Length; i++)
            {
                result += DeCodeForRemoteSite(word[i]);
            }

            return result;
        }


    }
}