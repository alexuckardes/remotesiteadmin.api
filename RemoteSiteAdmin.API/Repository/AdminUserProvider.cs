﻿using RemoteSiteAdmin.API.Persistence;
using RemoteSiteAdmin.API.Persistence.DbModels;
using RemoteSiteAdmin.API.Requests;
using RemoteSiteAdmin.API.Responses;
using System.Linq;

namespace RemoteSiteAdmin.API.Providers
{
    public class AdminUserProvider : IAdminUserProvider
    {
        private readonly AcmDbContext _dbContext;

        public AdminUserProvider(AcmDbContext dbContext)
        {
            _dbContext = dbContext;
        }
        public AdminUserLogInResponse LogIn(AdminUserLogInRequest AdminUserLogInRequest)
        {
            City city = _dbContext.Cities.FirstOrDefault();
            return new AdminUserLogInResponse();
        }
    }
}