﻿using RemoteSiteAdmin.API.Requests;
using RemoteSiteAdmin.API.Responses;

namespace RemoteSiteAdmin.API.Providers
{
    public interface IAdminUserProvider
    {
         AdminUserLogInResponse LogIn(AdminUserLogInRequest AdminUserLogInRequest);
    }
}
