﻿using RemoteSiteAdmin.API.Persistence;
using RemoteSiteAdmin.API.Persistence.DbModels;
using RemoteSiteAdmin.API.Responses;

namespace RemoteSiteAdmin.API.Providers
{
    public class RemoteSiteRepository : IRemoteSiteRepository
    {
        public CreateRemoteSiteResponse CreateRemoteSite(RemoteSite remoteSite)
        {
            using(AcmDbContext acmDbContext = new AcmDbContext())
            {
                acmDbContext.RemoteSites.Add(remoteSite);
                acmDbContext.SaveChanges();
                CreateRemoteSiteResponse result = new CreateRemoteSiteResponse()
                {
                    Messages = "Operation succeed"
                };

                return result;
            }
        }
    }
}