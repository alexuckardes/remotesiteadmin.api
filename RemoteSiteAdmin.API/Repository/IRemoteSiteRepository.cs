﻿using RemoteSiteAdmin.API.Persistence.DbModels;
using RemoteSiteAdmin.API.Responses;

namespace RemoteSiteAdmin.API.Providers
{
    public interface IRemoteSiteRepository
    {
        CreateRemoteSiteResponse CreateRemoteSite(RemoteSite remoteSite);
    }
}
