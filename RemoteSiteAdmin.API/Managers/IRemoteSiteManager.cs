﻿using RemoteSiteAdmin.API.Requests;
using RemoteSiteAdmin.API.Responses;

namespace RemoteSiteAdmin.API.Managers
{
    public interface IRemoteSiteManager
    {
        CreateRemoteSiteResponse CreateRemoteSIte(CreateRemoteSiteRequest createRemoteSiteRequest);
    }
}
