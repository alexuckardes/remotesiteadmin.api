﻿using RemoteSiteAdmin.API.Persistence.DbModels;
using RemoteSiteAdmin.API.Providers;
using RemoteSiteAdmin.API.Requests;
using RemoteSiteAdmin.API.Responses;
using System;

namespace RemoteSiteAdmin.API.Managers
{
    public class RemoteSiteManager : IRemoteSiteManager
    {
        private readonly IRemoteSiteRepository _remoteSiteRepository;

        public RemoteSiteManager(IRemoteSiteRepository remoteSiteRepository)
        {
            _remoteSiteRepository = remoteSiteRepository;
        }

        public CreateRemoteSiteResponse CreateRemoteSIte(CreateRemoteSiteRequest createRemoteSiteRequest)
        {
            RemoteSite remoteSite = new RemoteSite()
            {
                Name = createRemoteSiteRequest.RemoteSiteName,
                LogOnSince = DateTime.Now
            };
            CreateRemoteSiteResponse result = _remoteSiteRepository.CreateRemoteSite(remoteSite);

            return result;
        }
    }
}