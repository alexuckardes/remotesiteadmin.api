﻿using System.Collections.Generic;
using System.Web.Http;
using System.Net.Http;
using RemoteSiteAdmin.API.Requests;
using RemoteSiteAdmin.API.Responses;
using RemoteSiteAdmin.API.Providers;
using RemoteSiteAdmin.API.Managers;
using System.Net;

namespace RemoteSiteAdmin.API.Controllers
{
    [RoutePrefix("remotesite")]
    public class RemoteSiteController : ApiController
    {
        private readonly IRemoteSiteManager _remoteSiteManager;
        private readonly IRemoteSiteRepository _remoteSiteRepository;

        public RemoteSiteController(IRemoteSiteManager remoteSiteManager, IRemoteSiteRepository remoteSiteRepository)
        {
            _remoteSiteManager = remoteSiteManager;
            _remoteSiteRepository = remoteSiteRepository;
        }
        
        // GET: api/AdminUser/5
        //[AllowAnonymous]
        //[HttpGet]
        //[Route("countrylist/{id:int}")]
        //public HttpResponseMessage GetCountries(int id)
        //{
        //    //_adminUserProvider.LogIn(new AdminUserLogInRequest
        //    //{
        //    //    UserName = "asda",
        //    //    PassWord = "2123232"
        //    //});
        //    return Request.CreateResponse(System.Net.HttpStatusCode.OK, id);
        //}

        [AllowAnonymous]
        [HttpPost]
        [Route("create")]
        public HttpResponseMessage CreateRemoteSite(CreateRemoteSiteRequest createRemoteSiteRequest)
        {
            CreateRemoteSiteResponse response = _remoteSiteManager.CreateRemoteSIte(createRemoteSiteRequest);
            if(!response.HasError)
            {
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
        }

        // PUT: api/AdminUser/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/AdminUser/5
        public void Delete(int id)
        {
        }
    }
}
