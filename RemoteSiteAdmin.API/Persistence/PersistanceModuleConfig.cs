﻿using Autofac;
using System.Data.Entity;

namespace RemoteSiteAdmin.API.Persistence
{
    public class PersistanceModuleConfig : AbstractModuleConfig
    {
        public PersistanceModuleConfig(bool appHasPerRequestLifetimeSupport) : base(appHasPerRequestLifetimeSupport)
        {
        }

        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<AcmDbContext>()
                .AsSelf()
                .As<DbContext>()
                .InstancePerRequestOrLifetimeScope(AppHasPerRequestLifetimeSupport);
        }
    }
}