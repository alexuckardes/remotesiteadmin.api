﻿using RemoteSiteAdmin.API.Persistence.DbModels;
using System.Data.Entity;

namespace RemoteSiteAdmin.API.Persistence
{
    public class AcmDbContext : DbContext
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<RemoteSite> RemoteSites { get; set; }

        static AcmDbContext()
        {
            Database.SetInitializer<AcmDbContext>(null);
        }

        public AcmDbContext(string conectionString = "Name=AcmDbContext")
            : base(conectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }


    }
}