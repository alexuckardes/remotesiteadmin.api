﻿using System;

namespace RemoteSiteAdmin.API.Persistence.DbModels
{
    public class RemoteSite
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime LogOnSince { get; set; }
    }
}