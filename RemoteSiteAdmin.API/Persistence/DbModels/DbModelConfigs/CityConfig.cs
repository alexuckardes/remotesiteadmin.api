﻿using System.Data.Entity.ModelConfiguration;

namespace RemoteSiteAdmin.API.Persistence.DbModels.DbModelConfigs
{
    public class CityConfig : EntityTypeConfiguration<City>
    {
        public CityConfig()
        {
            ToTable("Cities");
            HasKey(c => c.Id);
            Property(c => c.Id).HasColumnName("ID");
            //Property((System.Linq.Expressions.Expression<System.Func<RemoteSite, string>>)(x => (string)x.Name)).HasColumnName("City").IsRequired().IsUnicode().HasMaxLength(20);
        }
    }
}