﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace RemoteSiteAdmin.API.Persistence.DbModels.DbModelConfigs
{
    public class RemoteSiteConfig : EntityTypeConfiguration<RemoteSite>
    {
        public RemoteSiteConfig()
        {
            ToTable("RemoteSites");
            HasKey(r => r.Id);
        }
    }
}