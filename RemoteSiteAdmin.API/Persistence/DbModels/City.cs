﻿namespace RemoteSiteAdmin.API.Persistence.DbModels
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int State { get; set; }
    }
}